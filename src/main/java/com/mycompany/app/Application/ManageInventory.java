package com.mycompany.app.Application;

import com.mycompany.app.domain.AbstractProduct;

import java.util.ArrayList;
import java.util.List;

public class ManageInventory {
    private List<AbstractProduct> products;

    public ManageInventory() {
        this.products = new ArrayList<>();
    }

    public List<AbstractProduct> getProducts() {
        return products;
    }

    public void showInventory() {
        System.out.println("Total products: " + products.size());
        System.out.println("Inventario:");
        for (AbstractProduct product : products) {
            System.out.println(product.getGeneralInformation() + " and " + product.getSpecificInformation());
        }
        System.out.println();
    }

    public void addProduct(AbstractProduct producto) {
        this.products.add(producto);
    }

    public void updateStockProduct(String productName, String size, int quantity) {
        for (AbstractProduct product : products) {
            if (product.getName().equals(productName) && product.getSize().equals(size)) {
                product.setStock(quantity);
                break;
            }
        }
    }

    public void removeProduct(String productName, String size) {
        this.products.removeIf(product -> product.getName().equals(productName) && product.getSize().equals(size));
    }
}
