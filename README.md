# CI-example


## Getting started

The proyect is a example about Gitlab CI, is a simple java application but the idea is verify the behivor of gitlab CI

You can see the pipelines and the structure for .gitlab-ci.yml file for understand the idea of repository

## Test
For running test execute the next command: `mvn test`

## Run
For running the proyect execute the next command: 
`mvn compile`
`mvn exec:java -Dexec.mainClass=com.mycompany.app.App`

## Author
Did with love for Julian Allegretti for certified in Software Arquitecture at Universidad de la sabana