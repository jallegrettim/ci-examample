package com.mycompany.app.domain;

public abstract class AbstractProduct {
    private String name;
    private String branch;
    private int stock;

    public AbstractProduct(String name, String branch, int stock) {
        this.name = name;
        this.branch = branch;
        this.stock = stock;
    }

    public int getCount() {
        return this.stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public String getGeneralInformation() {
        return "Name: " + this.name + ", Branch: " + this.branch + ", Stock: " + this.stock;
    }

    public abstract String getSpecificInformation();
    public abstract String getSize();

    public String getName() {
        return name;
    }
}
