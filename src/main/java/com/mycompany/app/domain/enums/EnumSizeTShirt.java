package com.mycompany.app.domain.enums;

public enum EnumSizeTShirt {
    XS, S, M, L, XL
}
