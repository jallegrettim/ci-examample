package com.mycompany.app.domain;

public class Sneaker extends AbstractProduct {
    private int size;

    public Sneaker(String name, String branch, int stock, int size) {
        super(name, branch, stock);
        this.size = size;
    }

    @Override
    public String getSpecificInformation() {
        return "Size of Sneaker: " + size;
    }

    @Override
    public String getSize() {
        return String.valueOf(this.size);
    }
}
