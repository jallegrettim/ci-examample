package com.mycompany.app.Application;

import com.mycompany.app.domain.Sneaker;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ManageInventoryTest {
    @Test
    public void testAddProduct() {
        ManageInventory manageInventory = new ManageInventory();
        manageInventory.addProduct(new Sneaker("Nike SB", "Nike", 80, 39));
        assertEquals(1, manageInventory.getProducts().size());
    }

    @Test
    public void testUpdateStockProduct() {
        ManageInventory manageInventory = new ManageInventory();
        manageInventory.addProduct(new Sneaker("Nike SB", "Nike", 80, 40));
        manageInventory.addProduct(new Sneaker("Nike SB", "Nike", 80, 38));
        assertEquals(80, manageInventory.getProducts().get(0).getCount());
        manageInventory.updateStockProduct("Nike SB", "38", 20);
        assertEquals(80, manageInventory.getProducts().get(0).getCount());
        assertEquals(20, manageInventory.getProducts().get(1).getCount());
    }

    @Test
    public void testRemoveProduct() {
        ManageInventory manageInventory = new ManageInventory();
        manageInventory.addProduct(new Sneaker("Nike SB", "Nike", 80, 40));
        manageInventory.addProduct(new Sneaker("Nike SB", "Nike", 80, 38));
        assertEquals(2, manageInventory.getProducts().size());
        manageInventory.removeProduct("Nike SB", "38");
        assertEquals(1, manageInventory.getProducts().size());
    }
}
