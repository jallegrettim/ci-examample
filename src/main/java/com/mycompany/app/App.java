package com.mycompany.app;

import com.mycompany.app.Application.ManageInventory;
import com.mycompany.app.domain.Sneaker;
import com.mycompany.app.domain.TShirt;
import com.mycompany.app.domain.enums.EnumSizeTShirt;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        ManageInventory manageInventory = new ManageInventory();

        manageInventory.addProduct(new Sneaker("Adidas Running", "Adidas", 10, 40));
        manageInventory.addProduct(new Sneaker("Adidas Samba", "Adidas", 20, 41));
        manageInventory.addProduct(new Sneaker("Adidas Samba", "Adidas", 20, 36));
        manageInventory.addProduct(new TShirt("Polo Cafe", "Levis", 40, EnumSizeTShirt.M));
        manageInventory.addProduct(new TShirt("Polo Gris", "Levis", 5, EnumSizeTShirt.L));

        manageInventory.showInventory();

        manageInventory.updateStockProduct("Adidas Samba", "41", 50);

        manageInventory.showInventory();

        manageInventory.removeProduct("Adidas Samba", "36");

        manageInventory.showInventory();
    }
}
