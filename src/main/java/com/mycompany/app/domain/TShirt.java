package com.mycompany.app.domain;

import com.mycompany.app.domain.enums.EnumSizeTShirt;

public class TShirt extends AbstractProduct {

    private EnumSizeTShirt size;

    public TShirt(String name, String branch, int stock, EnumSizeTShirt size) {
        super(name, branch, stock);
        this.size = size;
    }

    @Override
    public String getSpecificInformation() {
        return "Size of Shirt: " + this.size;
    }

    @Override
    public String getSize() {
        return this.size.toString();
    }

}
